# Button Component Page #

## Solution for a challenge from [Devchallenges.io](http://devchallenges.io). ##


## [Demo](https://affectionate-volhard-9db478.netlify.app/) | [Solution](https://devchallenges.io/solutions/goq8CTAzCE3blgXa72ga) | [Challenge](https://devchallenges.io/challenges/ohgVTyJCbm5OZyTB2gNY) ##

## Table of Contents

- [Overview](#overview)
- [Built With](#built-with)
- [Features](#features)
- [Contact](#contact)

## Overview

![screenshot](https://devchallenges.io/_next/image?url=https%3A%2F%2Ffirebasestorage.googleapis.com%2Fv0%2Fb%2Fdevchallenges-1234.appspot.com%2Fo%2FchallengesDesigns%252FButtonThumbnail.png%3Falt%3Dmedia%26token%3D3ddbedcf-a08b-4144-928f-e551b4bcee80&w=1920&q=75)

This application/site was created as a submission to a [DevChallenges](https://devchallenges.io/challenges) challenge.
The [challenge](https://devchallenges.io/challenges/ohgVTyJCbm5OZyTB2gNY) was to build an application to complete the given user stories.

## Built With

- [ReactJS](https://reactjs.org/)
- [Styled Components](https://styled-components.com/)

## Features

This project features a responsive page showcasing a Button Component with several states and props. Built with ReactJS and Styled Components.

## Contact

You can contact me [here](https://linktr.ee/rodrigodev).
