import styled from 'styled-components';

export const Container = styled.div`
  height: 100%;
  width: 100%;
  
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: stretch;
  overflow-x: hidden;
`;

export const NavigationMenu = styled.aside`
  height: 100%;
  width: 100%;
  min-width: 237px;
  max-width: 237px;
  background-color: #FAFBFD;

  display: flex;
  flex-direction: column;
  align-items: flex-start;
  justify-content: flex-start;

  @media(max-width: 1024px) {
    display: none;
  }
`;

export const LogoContainer = styled.div`
  height: auto;
  width: auto;

  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: center;
  margin: 47px 66px 122px 56px;
`;

export const Logo1 = styled.span`
  font-family: "Poppins", sans-serif;
  font-style: normal;
  font-weight: 600;
  font-size: 13px;
  line-height: 19px;

  color: #F7542E;
`;

export const Logo2 = styled.span`
  font-family: "Poppins", sans-serif;
  font-style: normal;
  font-weight: 600;
  font-size: 13px;
  line-height: 19px;

  color: #000000;
`;

export const List = styled.ul`
  padding: 0 0 0 56px;
  max-height: 350px;
  height: 100%;
  width: 100%;
  list-style: none;
  display: flex;
  flex-direction: column;
  align-items: flex-start;
  justify-content: space-between;
`;

export const Item = styled.li`
  font-family: "Noto Sans JP";
  font-style: normal;
  font-weight: 500;
  font-size: 14px;
  line-height: 20px;
  color: #9E9E9E;
  cursor: pointer;
`;

export const Selected = styled.strong`
  font-weight: 700;
  color: #090F31;
`;