import React from "react";
import Content from "../../components/Content";

import {
  Container,
  NavigationMenu,
  LogoContainer,
  Logo1,
  Logo2,
  List,
  Item,
  Selected
} from "./styles";

const Home = () => {
  return (
    <Container>
      <NavigationMenu>
        <LogoContainer>
          <Logo1>Dev</Logo1>
          <Logo2>challenges.io</Logo2>
        </LogoContainer>
        <List>
          <Item>Colors</Item>
          <Item>Typography</Item>
          <Item>Spaces</Item>
          <Item><Selected>Buttons</Selected></Item>
          <Item>Inputs</Item>
          <Item>Grid</Item>
        </List>
      </NavigationMenu>
      <Content />
    </Container>
  );
}

export default Home;