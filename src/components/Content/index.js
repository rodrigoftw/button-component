import React from 'react';
import Button from "../Button";
import Footer from "../Footer";

import {
  Container,
  Row,
  Column,
  TitleColumn,
  Title,
  ButtonCodeTitle,
  ButtonInfoTitle,
  IconsSpan,
  IconsLink
} from './styles';

function Content() {
  return (
    <Container>
      <Row>
        <TitleColumn>
          <Title>Buttons</Title>
        </TitleColumn>
      </Row>
      <Row>
        <Column>
          <ButtonCodeTitle>{"<Button />"}</ButtonCodeTitle>
          <Button
            title="Default"
          />
        </Column>
        <Column>
          <ButtonInfoTitle>{"&:hover, &:focus"}</ButtonInfoTitle>
          <Button
            title="Default"
            background="#AEAEAE"
          />
        </Column>
      </Row>
      <Row>
        <Column>
          <ButtonCodeTitle>{`<Button variant="outline" />`}</ButtonCodeTitle>
          <Button
            variant="outline"
            background="none"
            disableShadow
            title="Default"
          />
        </Column>
        <Column>
          <ButtonInfoTitle>{"&:hover, &:focus"}</ButtonInfoTitle>
          <Button
            variant="outline"
            title="Default"
            disableShadow
            background="rgba(41, 98, 255, 0.1) !important" />
        </Column>
      </Row>
      <Row>
        <Column>
          <ButtonCodeTitle>{`<Button variant="text" />`}</ButtonCodeTitle>
          <Button
            variant="text"
            disableShadow
            title="Default"
          />
        </Column>
        <Column>
          <ButtonInfoTitle>{"&:hover, &:focus"}</ButtonInfoTitle>
          <Button
            variant="text"
            disableShadow
            title="Default"
            background="rgba(41, 98, 255, 0.1) !important"
          />
        </Column>
      </Row>
      <Row>
        <Column>
          <ButtonCodeTitle>{`<Button disableShadow />`}</ButtonCodeTitle>
          <Button background="#2962FF" color="primary" disableShadow title="Default" />
        </Column>
      </Row>
      <Row>
        <Column>
          <ButtonCodeTitle>{`<Button disabled />`}</ButtonCodeTitle>
          <Button disabled disableShadow title="Disabled" />
        </Column>
        <Column>
          <ButtonCodeTitle>{`<Button variant="text" disabled />`}</ButtonCodeTitle>
          <Button disabled variant="text" title="Disabled" />
        </Column>
      </Row>
      <Row>
        <Column>
          <ButtonCodeTitle>{`<Button startIcon="local_grocery_store" />`}</ButtonCodeTitle>
          <Button background="#2962FF" color="primary" startIcon="add_shopping_cart" title="Default" />
        </Column>
        <Column>
          <ButtonCodeTitle>{`<Button endIcon="local_grocery_store" />`}</ButtonCodeTitle>
          <Button background="#2962FF" color="primary" endIcon="add_shopping_cart" title="Default" />
        </Column>
      </Row>
      <Row>
        <Column>
          <ButtonCodeTitle>{`<Button size="sm" />`}</ButtonCodeTitle>
          <Button background="#2962FF" color="primary" size="sm" title="Default" />
        </Column>
        <Column>
          <ButtonCodeTitle>{`<Button size="md" />`}</ButtonCodeTitle>
          <Button background="#2962FF" color="primary" size="md" title="Default" />
        </Column>
        <Column>
          <ButtonCodeTitle>{`<Button size="lg" />`}</ButtonCodeTitle>
          <Button background="#2962FF" color="primary" size="lg" title="Default" />
        </Column>
      </Row>
      <Row>
        <Column>
          <ButtonCodeTitle>{`<Button color="default" />`}</ButtonCodeTitle>
          <Button color="default" title="Default" />
        </Column>
        <Column>
          <ButtonCodeTitle>{`<Button color="primary" />`}</ButtonCodeTitle>
          <Button color="primary" title="Primary" />
        </Column>
        <Column>
          <ButtonCodeTitle>{`<Button color="secondary" />`}</ButtonCodeTitle>
          <Button color="secondary" title="Secondary" />
        </Column>
        <Column>
          <ButtonCodeTitle>{`<Button color="danger" />`}</ButtonCodeTitle>
          <Button color="danger" title="Danger" />
        </Column>
      </Row>
      <Row>
        <Column>
          <ButtonInfoTitle>{`&:hover, &:focus`}</ButtonInfoTitle>
          <Button background="#AEAEAE !important" color="default" title="Default" />
        </Column>
        <Column>
          <ButtonInfoTitle>{`&:hover, &:focus`}</ButtonInfoTitle>
          <Button background="#0039CB !important" color="primary" title="Primary" />
        </Column>
        <Column>
          <ButtonInfoTitle>{`&:hover, &:focus`}</ButtonInfoTitle>
          <Button background="#1C313A !important" color="secondary" title="Secondary" />
        </Column>
        <Column>
          <ButtonInfoTitle>{`&:hover, &:focus`}</ButtonInfoTitle>
          <Button background="#9A0007 !important" color="danger" title="Danger" />
        </Column>
      </Row>
      <Row>
        <Column>
          <IconsSpan>
            Icons:
            <IconsLink
              href="https://material.io/resources/icons/?style=round"
              target="_blank"
              rel="noopener noreferrer"
            >
              https://material.io/resources/icons/?style=round
            </IconsLink>
          </IconsSpan>
        </Column>
      </Row>
      <Footer />
    </Container>
  );
}

export default Content;