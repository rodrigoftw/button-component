import styled from 'styled-components';

export const Container = styled.div`
  height: 100%;
  max-width: 786px;
  width: 100%;
  display: flex;
  flex-direction: column;
  align-items: stretch;
  justify-content: center;
  flex-grow: 1;
`;

export const Row = styled.div`
  height: auto;
  width: auto;

  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: flex-start;
  margin: 0 0 0 64px;
  
  @media(max-width: 1439px) {
    flex-wrap: wrap;
  }
  
  @media(max-width: 768px) {
    justify-content: center;
    margin: 0;
  }
`;

export const Column = styled.div`
  height: 80px;
  max-width: 400px;
  width: 100%;
  min-width: 200px;
  padding: 24px 48px 24px 0;
  
  display: flex;
  flex-direction: column;
  align-items: flex-start;
  justify-content: space-evenly;
  
  @media(max-width: 1439px) {
    width: auto;
  }

  @media(max-width: 768px) {
    width: 100%;
    padding: 16px;
    align-items: center;
  }
`;

export const TitleColumn = styled.div`
  height: 36px;
  width: 100%;
  padding: 53px 0 32px;

  display: flex;
  flex-direction: column;
  align-items: flex-start;
  justify-content: space-evenly;

  @media(max-width: 768px) {
    width: 100%;
    align-items: center;
    justify-content: center;
  }
`;

export const Title = styled.span`
  font-family: "Poppins", sans-serif;
  font-style: normal;
  font-weight: 500;
  font-size: 24px;
  line-height: 36px;

  color: #4F4F4F;
`;

export const ButtonCodeTitle = styled.span`
  font-family: "Ubuntu Mono", monospace;
  font-style: normal;
  font-weight: 400;
  font-size: 12px;
  line-height: 12px;

  color: #333333;
`;

export const ButtonInfoTitle = styled.span`
  font-family: "Ubuntu Mono", monospace;
  font-style: normal;
  font-weight: 400;
  font-size: 12px;
  line-height: 12px;
  text-align: center;

  color: #828282;
`;

export const IconsSpan = styled.span`
  font-family: "Ubuntu Mono", monospace;
  font-style: normal;
  font-weight: normal;
  font-size: 12px;
  line-height: 12px;
  
  color: #828282;
  
  @media(max-width: 768px) {
    text-align: center;
  }
`;

export const IconsLink = styled.a`
  margin-left: 8px;
  text-decoration: none;
  color: #828282;
`;