import styled, { css } from 'styled-components';

export const Button = styled.button`
  min-width: 81px;
  width: auto;
  height: 36px;
  cursor: pointer;
  padding: 8px 16px;
  text-align: center;
  color: ${props => props.color ? props.color : "#3F3F3F"};

  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: center;
  background: ${props => props.background ? props.background : "#E0E0E0"};
  border: ${props => props.variant ? props.variant : "none"};
  border-radius: 6px;
  box-shadow: ${props => props.disableShadow ? "none" : "0px 2px 3px rgba(51, 51, 51, 0.2)"};
  transition: background 0.4s;

  :hover,
  :focus {
    background: ${props => props.background ? props.background : "#AEAEAE"};
    outline: none;
  }
  
  ${(props) =>
    !props.variant &&
    css`
      border: none;
    `
  }

  ${(props) =>
    (props.variant === "outline") &&
    css`
      background: transparent;
      border: 1px solid #3D5AFE;
      color: #3D5AFE;
      :hover,
      :focus {
        background: rgba(41, 98, 255, 0.1);
        outline: none;
      }
    `
  }

  ${(props) =>
    (props.variant === "text") &&
    css`
      background: transparent;
      border: none;
      box-shadow: none;
      color: #3D5AFE;
      :hover,
      :focus {
        background: rgba(41, 98, 255, 0.1);
        outline: none;
      }
    `
  }

  ${(props) =>
    props.disabled &&
    css`
      background: #E0E0E0;
      border: none;
      color: #9E9E9E;
      :hover,
      :focus {
        background: #E0E0E0;
      }
      cursor: auto;
      outline: none;
    `
  }

  ${(props) =>
    (props.variant === "text" &&
      props.disabled) &&
    css`
      background: transparent;
      border: none;
      color: #9E9E9E;
      :hover,
      :focus {
        background: transparent;
      }
      cursor: auto;
      outline: none;
    `
  }

  ${(props) =>
    (props.size === "sm") &&
    css`
      min-width: 73px;
      width: auto;
      height: 32px;
      padding: 6px 12px;
    `
  }

  ${(props) =>
    (props.size === "md") &&
    css`
      min-width: 81px;
      width: auto;
      height: 36px;
      padding: 8px 16px;
    `
  }

  ${(props) =>
    (props.size === "lg") &&
    css`
      min-width: 93px;
      width: auto;
      height: 42px;
      padding: 11px 22px;
    `
  }

  ${(props) =>
    props.color === "default" &&
    css`
      color: #3F3F3F;
      background: #E0E0E0;
      :hover,
      :focus {
        background: #AEAEAE;
      }
    `
  }

  ${(props) =>
    props.color === "primary" &&
    css`
      color: #FFFFFF;
      background: #2962FF;
      :hover,
      :focus {
        background: #0039CB;
      }
    `
  }

  ${(props) =>
    props.color === "secondary" &&
    css`
      color: #FFFFFF;
      background: #455A64;
      :hover,
      :focus {
        background: #1C313A;
      }
    `
  }

  ${(props) =>
    props.color === "danger" &&
    css`
      color: #FFFFFF;
      background: #D32F2F;
      :hover,
      :focus {
        background: #9A0007;
      }
    `
  }
`;

export const Title = styled.span`
  font-family: "Noto Sans JP", sans-serif;
  font-style: normal;
  font-weight: 500;
  font-size: 14px;
  line-height: 20px;
`;