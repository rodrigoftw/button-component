import React from 'react';

import { Button, Title } from './styles';

function CustomButton({
  title,
  background,
  variant,
  disableShadow,
  disabled,
  startIcon,
  endIcon,
  color,
  size
}) {
  return (
    <Button
      background={background}
      variant={variant}
      disableShadow={disableShadow}
      disabled={disabled}
      color={color}
      size={size}
    >
      {startIcon && (
        <span
          className="material-icons"
          style={{ marginRight: "8px" }}
        >
          {startIcon}
        </span>
      )}
      <Title>{title}</Title>
      {endIcon && (
        <span
          className="material-icons"
          style={{ marginLeft: "8px" }}
        >
          {endIcon}
        </span>
      )}
    </Button>
  );
}

export default CustomButton;